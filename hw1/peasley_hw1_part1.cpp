#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

//very hack but it works and I don't feel like working on this more
int print = 0;
void forkem(int numloops){

	for(int i = 0; i < 3; i++){
		pid_t parent = getpid();
		pid_t pid = fork();

		if(getpid() == parent){
			for(int i = 0; i < numloops; i++){
				if(print < numloops){
			 	printf("This is the main process, my PID is %d\n",getpid());
			 	sleep(2);
			 	printf("This is the main process, my PID is %d\n",getpid());
			 	print++;
			 }
			}
		}
		//Probably a better way to do this than loop hell
		if(pid != 0){
			continue;
		}
		if(i == 0){
			for(int i = 0; i < numloops; i++){
				printf("This is a child process, my PID is %d, my Parent PID is %d\n",getpid(),getppid());
				sleep(2);
				printf("This is a child process, my PID is %d, my Parent PID is %d\n",getpid(),getppid());
			}
			exit(0);
		}
		if(i == 1){
			for(int i = 0; i < numloops; i++){
				printf("This is a child process, my PID is %d, my Parent PID is %d\n",getpid(),getppid());
				sleep(2);
				printf("This is a child process, my PID is %d, my Parent PID is %d\n",getpid(),getppid());
			}
		exit(0);
		}
		if(i == 2){
			for(int i = 0; i < numloops; i++){
				printf("This is a child process, my PID is %d, my Parent PID is %d\n",getpid(),getppid());
				sleep(2);
				printf("This is a child process, my PID is %d, my Parent PID is %d\n",getpid(),getppid());
			}
		exit(0);
		}
	}

	//This will wait to exit the main program until all the children exit
	pid_t stop;
	int forme = 0;
	while ((stop = wait(&forme)) > 0);

}

int main (int argc, char *argv[]) {
		std::string failm = "Please enter an integer value from 1 to n, the number of times to print out a process and it's 3 children two times";

		//
		if(argc != 2){
			std::cout << failm;
			return 0;
		}
		char *numtimes = argv[1];
		int fail = !isdigit(*numtimes);
		if(fail){
			std::cout << failm;
			return 0;
		}
		if (atoi(numtimes) < 1) {
			std::cout << failm;
			return 0;
		}

    printf("forking\n");
		forkem(atoi(numtimes));

}
