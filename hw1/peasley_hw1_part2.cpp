#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void threeProc(std::string filename){

	std::cout <<" I am the main process, my PID is: " << getpid() << std::endl;
	for(int i = 0; i < 3; i++){
		pid_t pid = fork();
		if(pid != 0){
			continue;
		}
		else if(i == 0){
			execl("/bin/ls", "ls", "-l", (char *) 0);
			return;
		}
		else if(i == 1){
			execl("/bin/ps","ps","-ef", (char *) 0);
			return;
		}
		else if(i == 2){
			//const char *cfilename = filename.c_str();
			//execl("bin/cat", "cat", cfilename, (char *) 0);

			//The homework only mentioned to use execl for 'ls -l' and using execl for this
			//was giving me problems
			std::string com = "/bin/more " + filename;
			system(com.c_str());
			return;
		}
	}

	//This will wait to exit the main program until all the children exit
	pid_t stop;
	int forme = 0;
	while ((stop = wait(&forme)) > 0);
	std::cout << "main process terminates";
}
int main( int argc, char *argv[]){
	//If you enter an argument and it isn't a file, more's messages will display
	if(argc != 2){
		std::cout << "Please provide 1 file name to the program";
		return 0;
	}
	std::string filename = argv[1];
	threeProc(filename);
}


