#include <unistd.h>
#include <string>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <cstring>

#define PROCESS 0
#define FILE 1
/*
  Program 1

  Building:
    g++ 1.cpp -o 1

  This program will create two pipes, run ls with one, ps with another,
  and pipe the outputes into the file descripter, then into an array
  The program then gets the number of files and services from the array and
  attempts to send an email

*/

//Dumb globals
int arrayCount = 0;
int fileCount = 0;
int processCount = 0;
std::string *info = new std::string[400];


void ls(int pipefdp[2]){
  pid_t child1 = fork();
  if (child1 == 0){
    dup2(pipefdp[1],1);
    close(pipefdp[1]);
    close(pipefdp[0]);
    execl("/bin/ls", "/bin/ls", "-l", (char *) 0);
    exit(1);
  }
  else{
    return;
  }
}

void ps(int pipefdp2[2]){
  //Runs the ps command into our FD
  pid_t child2 = fork();
  if (child2 == 0){
    dup2(pipefdp2[1],1);
    close(pipefdp2[1]);
    close(pipefdp2[0]);
    execl("/bin/ps", "aux", (char *) 0);
    exit(1);
    }
  else{
    return;
  }
}

void readme(int pipefdp[2], int flag){
  //Reads out FD after each command (ps, aux) we run
  int bytes_read;
  char readbuffer[2];
  close(pipefdp[1]);
  std::string receive_output = "";
  while (1){
    bytes_read = read(pipefdp[0], readbuffer, sizeof(readbuffer)-1);
    if (bytes_read <= 0)
      break;
    receive_output += readbuffer;
    readbuffer[bytes_read] = '\0';
    
    if(readbuffer[0] == '\n'){
      //New line, add to array
      info[arrayCount] = receive_output;
      arrayCount++;
      receive_output = "";
      //Depending on the flag given, change increment
      if(flag == PROCESS)
        processCount++;
      else{
        fileCount++;  
      }
    }
  }
}

void removeJunk(){
  //Hacky way to remove junk header from our info array
  info[0] = "";
  info[processCount] = "";
}

void printAndMail(char* email){
  // Prints out our array and counts incase the email doesn't send
  std::cout << "Incase the mail doesn't work, see below \n";
  std::cout << "Processes: " << processCount-1 << std::endl;
  std::cout << "Files: " << fileCount-1 << std::endl;
  std::cout << "Files and process below, from my array \n";
  for(int i = 0; i < 20; i++){
    std::cout << info[i];
  }

  //To be able to compile on uc filespace we have to use functions from 30+ years ago
  char processCountString[1000];
  char fileCountString[1000];
  sprintf(fileCountString,"%d",fileCount);  
  sprintf(processCountString,"%d",processCount); 
  std::string subject = std::string("Processes: ") + processCountString + " Files: " + fileCountString;
  std::cout << "Attempting to send mail...\n"; 
  execl("/bin/mailx", "mailx", "-s", subject, email, " | echo e" ,NULL);
}

int main (int argc, char *argv[]) {
  std::string failm = "Please enter an email address";
  if(argc != 2){
    std::cout << failm;
    return 0;
  }
  char *email = argv[1];
  int fail = isdigit(*email);
  if(fail){
    std::cout << failm;
    return 0;
  }

  int pipefdp[2];
  if (pipe(pipefdp) != 0 ) {
    std::cout << "pipe broke";
  }
  int pipefdp2[2];
  if (pipe(pipefdp2) != 0 ) {
    std::cout << "pipe broke";
  }

  ps(pipefdp);
  readme(pipefdp,PROCESS);

  ls(pipefdp2);
  readme(pipefdp2,FILE);

  //Wait for out children to finish
  pid_t stop;
  int forme = 0;
  while ((stop = wait(&forme)) > 0);
  
  removeJunk();
  printAndMail(email);
  delete info;
}
