#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>

#define FILE_SIZE 11
#define LIMIT 20
#define FILE "/tmp/3.txt"

char* shared_buffer;


/*
  Program 3

  Building:
    gcc reader.c -o reader

  Reader:
    Reads from our shared memeory defined above 20 times then unlinks and quits
    This borrows a lot from our previous assignment, newrace.c,

*/

void reader(){
  int i,j,n;
  char results[FILE_SIZE];
  for (i=0; i<1; i++) {
    printf("Reader (pid = %d) arrives\n", getpid());
    for (j=0; j<FILE_SIZE; j++) {
      results[j] = shared_buffer[j];
    }
    printf("Reader %d gets results = %s\n", getpid(), results);
  }
  msync((void *)shared_buffer, FILE_SIZE, MS_SYNC);
}

void setUp(){
  int fd = open(FILE, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
  if ( fd < 0 ) {
    perror(FILE);
    exit(1);
  }
  char InitData[]="0000000000";
  write(fd, InitData, FILE_SIZE);
  // unlink(FILE);
  shared_buffer = mmap(0, FILE_SIZE, PROT_READ, MAP_SHARED, fd, 0);
  if ( shared_buffer == -1) {
    perror("mmap");
    exit(2);
  }
}

int main(){
  setUp();
  int runCount = 0;
  while(runCount < LIMIT){
    reader();
    sleep(2);
    runCount++;
    printf("Ran %d times\n",runCount);
  }
  unlink(FILE);
}