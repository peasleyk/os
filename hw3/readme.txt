Included in the zipfile is:
	Program 1: 1.cpp
	Program 2: server.cpp client.cpp
	Program 3: writer.c reader.c
Along with the built versions

Also included is a makefile,
	make 1: Build 1
	make 2: Builds server.cpp client.cpp
	make 3: Builds writer.c reader.c
	make all: Builds 1 2 3
	make clean: deletes build programs

Each program has a comment block that describes what it does at the beginning
