#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <math.h>

#define LIMIT 20
#define FILE_SIZE 11
#define FILE "/tmp/3.txt"

char* shared_buffer;

/*
  Program 3

  building:
    gcc writer.c -o writer -lm

  writer:
    This borrows a lot from our previous assignment, newrace.c,
    writes to our shared memory defined above 20 times then unlinks and quits.
*/


void writer()
{
  int i,j,n;
  char data[FILE_SIZE];

  //Gets our current PID and an int, gets the number of digits,
  //Copies it to a char array, gets the last entry in that char array
  int pid = getpid();
  char snum[10];
  sprintf(snum, "%Ld", pid);
  int length = floor(log10(pid)) +1;
  char lastPid[1];
  lastPid[0] = snum[length-1];


  for (j=0; j<FILE_SIZE-1; j++) {
      data[j]=  + lastPid[0];
  }
  data[j]= 0;

  for (i=0; i<1; i++) {
      printf("(pid = %d) arrives, writing %s to buffer\n", getpid(), data);

      for (j=0; j<FILE_SIZE-1; j++) {
          shared_buffer[j]= data[j];
      }
      printf("Finishes\n");
  }
  msync((void *)shared_buffer, FILE_SIZE, MS_SYNC);
}


void setUp(){
int fd = open(FILE, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
  if ( fd < 0 ) {
     perror(FILE);
     exit(1);
  }
  char InitData[]="0000000000";
  write(fd, InitData, FILE_SIZE);
  shared_buffer = mmap(0, FILE_SIZE, PROT_WRITE | PROT_WRITE, MAP_SHARED, fd, 0);
  //ReaderCount = mmap(0, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  if ( shared_buffer == -1) {
    perror("mmap");
    exit(2);
  }


}

int main(){
  setUp();
  int runCount = 0;
  while(runCount < LIMIT){
    writer();
    sleep(2);
    runCount++;
    printf("Ran %d times\n",runCount);
  }
  unlink(FILE);
}