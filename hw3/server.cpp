#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include  <sys/ipc.h>
#include  <sys/shm.h>
#include  <unistd.h>
#include  <string>
#include  <string.h>
#include  <iostream>
#include  <fstream>

#define CHANGED 1
#define CLEAN 0
#define FILENAME "sharedMessages.txt"
#define KEY 3212

/*
  Program 2

  Building:
    g++ server.cpp -o server

  Server:
    Running the server will start it in a forever loop
    It will continually check for new messages until ctrl-c is presses or 'STOP!!!' is sent
    On valid messages it will write to 'sharedMessages.txt'
    It will print out messages it receives and if it  writes it to buffer
    There is no timeout

*/


//Allows us to check the status of a message 
//And also pass a buffer
struct data {
  int  status;
  char buffer[1000];
};

void setupServer(int &SVSID, struct data *ServerData){
  key_t ShmKEY = KEY;
  SVSID = shmget(ShmKEY, 10000, IPC_CREAT | 0666);
}

void writeToFile(struct data *ServerData){
  //Opens a file and appends our buffer to it
  std::ofstream myfile;
  myfile.open(FILENAME, std::ofstream::out | std::ofstream::app);
  myfile << ServerData->buffer << std::endl;
  std::cout << "Wrote to file \n";
}

bool check(struct data *ServerData){
  //Check the buffer on every message to see if
  //It contains STOP!!! and exits if so
  int stopCount = 0;
  char stopWord[8] = "STOP!!!";
  for(int i = 0; i < 1000; i++){
    if(ServerData->buffer[i] == stopWord[stopCount]){
      stopCount++;
      if(stopCount == 8){
        return false;
      }
    }
  }

  return true;
}

void closeServer(struct data *ServerData){
  std::cout << "Stop word found in buffer, closing server\n";
  shmdt((void *) ServerData);
}

int  main(){
  int SVSID;
  struct data *ServerData;
  setupServer(SVSID,ServerData);
  ServerData = (struct data *) shmat(SVSID, NULL, 0);
  std::cout << "Server started correctly, data from client" <<
                " will be written to " << FILENAME << std::endl;
  ServerData->status = CLEAN;

  //Continually waits until we've changed our buffer,
  //then checks if it's time to stop, if it isn't
  //then write the buffer to the file
  while(true){
    while (ServerData->status != CHANGED);
    std::cout << "Found " << ServerData->buffer << std::endl;
    if(check(ServerData)){
      writeToFile(ServerData);
      ServerData->status = CLEAN;
    }
    else{
      closeServer(ServerData);
      return 0;
    }
   }
}