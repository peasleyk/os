#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include  <sys/ipc.h>
#include  <sys/shm.h>
#include  <string>
#include  <string.h>
#include  <iostream>

#define CHANGED 1
#define CLEAN 0
#define KEY 3212

/*
  Program 2

  Building:
    g++ client.cpp -o client

 CLient:
    Will only prompt for input after server is running
    It will ask for a message, if the message is > 1000 bytes it will exit without sending
    If it's valid, it send the message to the server and asks for another message
    If the server is down the client will hang until it's started again
    There is no timeout
*/

//Allows us to check the status of a message and also pass a buffer
struct data {
  int  status;
  char buffer[1000];
};

bool setupClient(int &SVCLID, struct data *ClientData){
  key_t ShmKEY = KEY;
  SVCLID = shmget(ShmKEY, 1000, 0666);
  if (SVCLID < 0) {
    std::cout << "Could not connect, is server running?" << std::endl;
    return false;
  }
  std::cout << "Connected to " << KEY << std::endl;
  return true;
}

void writeToBuffer(std::string message, struct data *ClientData){
  //Enforce 1000 byte limit we set in out buffer
  if(message.size() > 1000){
    std::cout << "Message is over 1000 bytes no message written" << std::endl;
  }
  else{
    strcpy(ClientData->buffer,message.c_str());
    std::cout << "Wrote message to buffer, notifying server if up" << std::endl;
    ClientData->status = CHANGED;
  }
}

int  main(){
  int SVCLID;
  struct data *ClientData;
  int status = setupClient(SVCLID, ClientData);
  if(!status){
    std::cout << "Could not connect, exiting";
  }
  ClientData = (struct data *) shmat(SVCLID, NULL, 0);

  while (true){
    while (ClientData->status != CLEAN);
    std::cout << "Server ready" << std::endl;
    std::string message;
    std::cout << "Enter a message to send to our server \n";
    std::getline(std::cin,message);
    writeToBuffer(message,ClientData);
  }
}